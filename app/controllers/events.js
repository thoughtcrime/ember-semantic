import Ember from 'ember';

export default Ember.Controller.extend({
  sortProperties: ['timestamp'],
  sortAscending: false,
  actions: {
    addEvent() {
      let newEvent = this.store.createRecord('event', {
        'icon': 'add to calendar center aligned icon',
        'title': this.get('title'),
        'host': this.get('host'),
        'description': this.get('description'),
        'street': this.get('street'),
        'city': this.get('city'),
        'state': this.get('state'),
        'zip': this.get('zip'),
        'attendees': [],
        'attending': false,
        'presenting': false,
        'type': 'Conference',
        'timestamp': new Date().getTime()
      });
      newEvent.save().transitionTo('events').catch(function(reason) {
        console.log(reason);
      });
    }
  }
});
