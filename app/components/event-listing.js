import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    toggleAttendance(event) {
      let attendees = event.get('attendees');
      let newAttendees = [];
      let newIcon = '';
      let isAttending = event.get('attending');
      attendees.forEach(function(attendee) {
        newAttendees.push(attendee);
      });
      if (newAttendees.indexOf('Shane') === -1) {
        newAttendees.push('Shane');
        newIcon = 'remove from calendar right aligned icon';
        isAttending = true;
      } else {
        newAttendees.splice(newAttendees.indexOf('Shane'), 1);
        newIcon = 'add to calendar centered icon';
        isAttending = false;
      }
      event.set('attendees', newAttendees);
      event.set('attending', isAttending);
      event.set('icon', newIcon);
    }
  }
});
