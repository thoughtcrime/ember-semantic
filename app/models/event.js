import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  icon: DS.attr(),
  title: DS.attr(),
  description: DS.attr(),
  attending: DS.attr(),
  presenting: DS.attr(),
  attendees: DS.attr(),
  host: DS.attr(),
  street: DS.attr(),
  city: DS.attr(),
  state: DS.attr(),
  zip: DS.attr(),
  type: DS.attr(),
  address: Ember.computed('street', 'city', 'state', 'zip', function() {
    return `${this.get('street')}, ${this.get('city')}, ${this.get('state')}  ${this.get('zip')}`;
  }),
  mapURL: Ember.computed('street', 'city', 'state', 'zip', function() {
    return `${this.get('street')}+${this.get('city')}+${this.get('state')}+${this.get('zip')}`;
  })
});
