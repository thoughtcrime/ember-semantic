export default function() {

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    http://www.ember-cli-mirage.com/docs/v0.2.x/shorthands/
  */
  this.get('/events', function() {
    return {
      data: [{
        type: 'events',
        id: 1,
        attributes: {
          icon: 'add to calendar center aligned icon',
          title: 'Leaders in Literacy Education',
          description: `The Dept. of Education's Annual Awards Banquet in honor of our state's leaders in Literacy Education.`,
          attending: false,
          presenting: false,
          attendees: ['Tom', 'Jane', 'Bill', 'Terrie', 'Annette', 'Marsha'],
          host: 'Arkansas Department of Education',
          street: `Four Capitol Mall Room 403-A`,
          city: `Little Rock`,
          state: `AR`,
          zip: `72201`,
          type: 'Banquet',
        }
      }, {
        type: 'events',
        id: 2,
        attributes: {
          icon: 'add to calendar centered icon',
          title: 'The Role of Classroom Teachers for Effective Assessment',
          description: `In this presentation, Dr. Hyde will present recent findings regarding the importance of obtaining accurate student assessment data during the course of daily classroom activities.`,
          attending: false,
          presenting: false,
          attendees: ['Tom', 'Jane', 'Bill', 'Terrie'],
          host: `Crowley's Ridge Educational Service Cooperative`,
          street: `1606 Pine Grove Ln`,
          city: `Harrisburg`,
          state: `AR`,
          zip: `72432`,
          type: 'Presentation',
        }
      }, {
        type: 'events',
        id: 3,
        attributes: {
          icon: 'add to calendar center aligned icon',
          title: 'Dyslexia Interventionist Training',
          description: 'Annual 5-day training required to receive certification as a K-12 Dyslexia Interventionist.',
          presenting: false,
          attending: true,
          attendees: ['Tom', 'Jane', 'Bill', 'Terrie', 'Annette', 'Marsha', 'Romeo', 'Juliet', 'William S.', 'Isaac N.', 'Albert', 'Marie', 'Martha', 'Eleanor', 'Hillary'],
          host: `Crowley's Ridge Educational Service Cooperative`,
          street: `1606 Pine Grove Ln`,
          city: `Harrisburg`,
          state: `AR`,
          zip: `72432`,
          type: 'Conference',
        }
      }]
    };
  });
}
